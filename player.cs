public class Player 
{
    const int initalBis = 10;
    public player()
    {
        this.mybarrel = new Barrel(initalBis);
    }
    public void takeBiscuit(Barrel barrelChosen)
    {
       barrelChosen.deduct();
    }
    public Barrel mybarrel { get; set; }
}

public class Barrel 
{
    public Barrel(int startNum)
    {
        this.buscuits = startNum;
    }
    public int buscuits { get; set; }

    public void deduct()
    {
        this.buscuits = this.buscuits--;
    }
}